﻿namespace DSC
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.number1 = new System.Windows.Forms.Button();
            this.number2 = new System.Windows.Forms.Button();
            this.number3 = new System.Windows.Forms.Button();
            this.number6 = new System.Windows.Forms.Button();
            this.number5 = new System.Windows.Forms.Button();
            this.number4 = new System.Windows.Forms.Button();
            this.number9 = new System.Windows.Forms.Button();
            this.number8 = new System.Windows.Forms.Button();
            this.number7 = new System.Windows.Forms.Button();
            this.number16C = new System.Windows.Forms.Button();
            this.number0 = new System.Windows.Forms.Button();
            this.DWdel = new System.Windows.Forms.Button();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.trackBar2 = new System.Windows.Forms.TrackBar();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
            this.SuspendLayout();
            // 
            // number1
            // 
            this.number1.Location = new System.Drawing.Point(331, 10);
            this.number1.Name = "number1";
            this.number1.Size = new System.Drawing.Size(75, 23);
            this.number1.TabIndex = 0;
            this.number1.Text = "1";
            this.number1.UseVisualStyleBackColor = true;
            this.number1.Click += new System.EventHandler(this.button1_Click);
            // 
            // number2
            // 
            this.number2.Location = new System.Drawing.Point(412, 10);
            this.number2.Name = "number2";
            this.number2.Size = new System.Drawing.Size(75, 23);
            this.number2.TabIndex = 1;
            this.number2.Text = "2";
            this.number2.UseVisualStyleBackColor = true;
            // 
            // number3
            // 
            this.number3.Location = new System.Drawing.Point(493, 10);
            this.number3.Name = "number3";
            this.number3.Size = new System.Drawing.Size(75, 23);
            this.number3.TabIndex = 2;
            this.number3.Text = "3";
            this.number3.UseVisualStyleBackColor = true;
            // 
            // number6
            // 
            this.number6.Location = new System.Drawing.Point(493, 39);
            this.number6.Name = "number6";
            this.number6.Size = new System.Drawing.Size(75, 23);
            this.number6.TabIndex = 5;
            this.number6.Text = "6";
            this.number6.UseVisualStyleBackColor = true;
            // 
            // number5
            // 
            this.number5.Location = new System.Drawing.Point(412, 39);
            this.number5.Name = "number5";
            this.number5.Size = new System.Drawing.Size(75, 23);
            this.number5.TabIndex = 4;
            this.number5.Text = "5";
            this.number5.UseVisualStyleBackColor = true;
            // 
            // number4
            // 
            this.number4.Location = new System.Drawing.Point(331, 39);
            this.number4.Name = "number4";
            this.number4.Size = new System.Drawing.Size(75, 23);
            this.number4.TabIndex = 3;
            this.number4.Text = "4";
            this.number4.UseVisualStyleBackColor = true;
            // 
            // number9
            // 
            this.number9.Location = new System.Drawing.Point(493, 68);
            this.number9.Name = "number9";
            this.number9.Size = new System.Drawing.Size(75, 23);
            this.number9.TabIndex = 8;
            this.number9.Text = "9";
            this.number9.UseVisualStyleBackColor = true;
            // 
            // number8
            // 
            this.number8.Location = new System.Drawing.Point(412, 68);
            this.number8.Name = "number8";
            this.number8.Size = new System.Drawing.Size(75, 23);
            this.number8.TabIndex = 7;
            this.number8.Text = "8";
            this.number8.UseVisualStyleBackColor = true;
            // 
            // number7
            // 
            this.number7.Location = new System.Drawing.Point(331, 68);
            this.number7.Name = "number7";
            this.number7.Size = new System.Drawing.Size(75, 23);
            this.number7.TabIndex = 6;
            this.number7.Text = "7";
            this.number7.UseVisualStyleBackColor = true;
            // 
            // number16C
            // 
            this.number16C.Location = new System.Drawing.Point(493, 97);
            this.number16C.Name = "number16C";
            this.number16C.Size = new System.Drawing.Size(75, 23);
            this.number16C.TabIndex = 11;
            this.number16C.Text = "16/C";
            this.number16C.UseVisualStyleBackColor = true;
            // 
            // number0
            // 
            this.number0.Location = new System.Drawing.Point(412, 97);
            this.number0.Name = "number0";
            this.number0.Size = new System.Drawing.Size(75, 23);
            this.number0.TabIndex = 10;
            this.number0.Text = "0";
            this.number0.UseVisualStyleBackColor = true;
            // 
            // DWdel
            // 
            this.DWdel.Location = new System.Drawing.Point(331, 97);
            this.DWdel.Name = "DWdel";
            this.DWdel.Size = new System.Drawing.Size(75, 23);
            this.DWdel.TabIndex = 9;
            this.DWdel.Text = "DW/del";
            this.DWdel.UseVisualStyleBackColor = true;
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(331, 126);
            this.trackBar1.Maximum = 100;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(237, 45);
            this.trackBar1.TabIndex = 12;
            // 
            // trackBar2
            // 
            this.trackBar2.Location = new System.Drawing.Point(331, 177);
            this.trackBar2.Maximum = 100;
            this.trackBar2.Name = "trackBar2";
            this.trackBar2.Size = new System.Drawing.Size(237, 45);
            this.trackBar2.TabIndex = 13;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(12, 12);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(313, 206);
            this.richTextBox1.TabIndex = 14;
            this.richTextBox1.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 266);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.trackBar2);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.number16C);
            this.Controls.Add(this.number0);
            this.Controls.Add(this.DWdel);
            this.Controls.Add(this.number9);
            this.Controls.Add(this.number8);
            this.Controls.Add(this.number7);
            this.Controls.Add(this.number6);
            this.Controls.Add(this.number5);
            this.Controls.Add(this.number4);
            this.Controls.Add(this.number3);
            this.Controls.Add(this.number2);
            this.Controls.Add(this.number1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button number1;
        private System.Windows.Forms.Button number2;
        private System.Windows.Forms.Button number3;
        private System.Windows.Forms.Button number6;
        private System.Windows.Forms.Button number5;
        private System.Windows.Forms.Button number4;
        private System.Windows.Forms.Button number9;
        private System.Windows.Forms.Button number8;
        private System.Windows.Forms.Button number7;
        private System.Windows.Forms.Button number16C;
        private System.Windows.Forms.Button number0;
        private System.Windows.Forms.Button DWdel;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.TrackBar trackBar2;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}

