﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSC.Models
{
    enum Frequency
    {
        Mf_2 = 2,
        Hf_4 = 4,
        Hf_6 = 6,
        Hf_8 = 8,
        Hf_12 = 12,
        Hf_16 = 16,
        VHF = 156
    };

    enum FormatSpecifier
    {
        Distress = 112,
        AllShips = 116,
        Group = 114,
        SellectiveIndividual = 120,
        GroupArea = 102,
        SellectiveCost = 123
    };

    enum GlobeSector
    {
        NE = 0,
        NW = 1,
        SE = 2,
        SW = 3
    };

    enum Category
    {
        Distress = 112,
        Urgent = 110,
        Safety = 108,
        Bussines = 106,
        Routine = 100
    };

    enum Distress
    {
        Fire = 100,
        Flooding = 101,
        Collision = 102,
        Grounding = 103,
        Listing = 104,
        Sinking = 105,
        Adrift = 106,
        Undesined = 107,
        Abandoning = 108,
        Piracy = 109,
        MOB = 110,
        EPIRBemission = 112
    };

    enum RespondCommunication
    {
        J3E = 109,
        F1B_J2B = 113,
        F3E_G3E_Simplex = 100
    };

    enum EoS
    {
        NeedConfirmation = 117,
        Response = 122,
        Etc = 127
    };

}
