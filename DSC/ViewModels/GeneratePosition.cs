﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSC.Models
{
    class GeneratePosition
    { 
        public string inDanger(GlobeSector gs, string widthDeg, string widthMin, string heightDeg, string heightMin)
        {
            string final = "";
            final += gs.ToString();
            final += parseNumber(widthDeg);
            final += parseNumber(widthMin);
            final += parseNumber(heightDeg);
            final += parseNumber(heightMin);
            return final;
        }

        public string Adress(GlobeSector gs, string widthDeg, string heightDeg, string vertical, string horizontal)
        {
            string final = "";
            final += gs.ToString();
            final += widthDeg;
            final += heightDeg;
            final += vertical;
            final += horizontal;
            return final;
        }

        public string inDanger()
        {
            return "9999999999";
        }

        private string parseNumber(string value)
        {
            int numbValue = int.Parse(value);
            string final = "";
            if (numbValue < 100 && numbValue > 9)
                final = "0" + value;
            else
                final = "00" + value;
            return value;
        }
    }
}
